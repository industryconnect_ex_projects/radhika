﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace test.Models
{
    public class MusicStoreEntities : DbContext
    {
        public MusicStoreEntities() : base("MusicStoreEntities"){ }


        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Role> Roles { get; set; }
    }
}