﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using test.Models;

namespace test.Controllers
{
    public class StoreController : Controller
    {
        MusicStoreEntities storeDB = new MusicStoreEntities();
                // GET: Store
         public ActionResult Index()
        {
            SampleData.Seed(storeDB);
            var genres = storeDB.Genres.ToList();
                return View(genres);
         
        }

        public ActionResult Browse(string genreName)
        {
            // var genremodel = new Genre { Name = genre };
            //var example = storeDB.Genres.Single(g => g.Name == "Disco");
            //var genreModel = storeDB.Genres.Include("Albums").Single(item => item.Name == genreName);

            return View();
        }

        public string Details()
        {
            return "Hello from Details";
        }
    }
}