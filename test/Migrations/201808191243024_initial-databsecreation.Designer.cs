// <auto-generated />
namespace test.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class initialdatabsecreation : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(initialdatabsecreation));
        
        string IMigrationMetadata.Id
        {
            get { return "201808191243024_initial-databsecreation"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
